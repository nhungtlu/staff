﻿export default {
  items: [
    {
      id: 'category1',
      title: 'Room',
    },
    {
      id: 'category2',
      title: 'Food & Drink',
    },
    {
      id: 'category3',
      title: 'Casio',
    },
    {
      id: 'category4',
      title: 'Airpost',
    },
    {
      id: 'category5',
      title: 'Fitnesscenter',
    },
    {
      id: 'category6',
      title: 'Hotel Linen Supply',
    },
  ],
};
