const COLORS = {
  white: '#FFF',
  dark: '#000',
  primary: '#52c0b4',
  secondary: '#e0f4f1',
  light: '#f9f9f9',
  grey: '#edebe8',
  orange: '#f5a623',
  yellow: '#fae500',
};

export default COLORS;
