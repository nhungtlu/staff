import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {DashBoardScreenNews} from '../screens/DashBoard/DashBoardNews';
import {DashBoardScreenRating} from '../screens/DashBoard/DashBoardRating';
import {DashBoardScreenReceivers} from '../screens/DashBoard/DashBoardReceivers';
import DashBoardScreen from '../screens/DashBoard/DashBoardScreen';
import MainTab from '../screens/MainTabScreen';
import NewsScreen from '../screens/News/NewsScreen';
import NewsScreenDetail from '../screens/News/NewsScreenDetail';
import {RateScreen} from '../screens/Rate';
import ReceiversScreenDetail from '../screens/Receivers/ReceiverScreenDetail';
import ReceiversScreen from '../screens/Receivers/ReceiversScreen';
import {MainStackParamList, TabStackParamList} from './Navigation';

const Stack = createStackNavigator<TabStackParamList & MainStackParamList>();
const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainTab"
        component={MainTab}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen name="News" component={NewsScreen} />
      <Stack.Screen name="NewsDetail" component={NewsScreenDetail} />
      <Stack.Screen name="Receivers" component={ReceiversScreen} />
      <Stack.Screen name="ReceiversDetail" component={ReceiversScreenDetail} />
      <Stack.Screen name="DashBoard" component={DashBoardScreen} />
      <Stack.Screen name="DashBoardNews" component={DashBoardScreenNews} />
      <Stack.Screen
        name="DashBoardReceivers"
        component={DashBoardScreenReceivers}
      />
      <Stack.Screen name="DashBoardRating" component={DashBoardScreenRating} />
      <Stack.Screen name="Rate" component={RateScreen} />
    </Stack.Navigator>
  );
};

export default MainStack;
