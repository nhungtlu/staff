import React from 'react';
import {StyleSheet} from 'react-native';
import {MainStackParamList, TabStackScreenProps} from '../stacks/Navigation';
import {
  BottomTabBarOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ReceiversScreen from './Receivers/ReceiversScreen';
import DashBoardScreen from './DashBoard/DashBoardScreen';
import NewsScreen from './News/NewsScreen';

const Tab = createBottomTabNavigator<MainStackParamList>();
const styles = StyleSheet.create({
  tabBarOption: {
    alignItems: 'stretch',
  },
});
const tabBarOptions: BottomTabBarOptions = {
  activeTintColor: 'red',
  showLabel: true,
  style: styles.tabBarOption,
};

export const MainTab: React.FC<TabStackScreenProps<'MainTab'>> = props => {
  console.log(props);
  return (
    <Tab.Navigator tabBarOptions={tabBarOptions}>
      <Tab.Screen
        name="News"
        component={NewsScreen}
        options={{
          tabBarLabel: 'News',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="newspaper"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Receivers"
        component={ReceiversScreen}
        options={{
          tabBarLabel: 'Receivers',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="chat" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="DashBoard"
        component={DashBoardScreen}
        options={{
          tabBarLabel: 'DashBoard',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="desktop-mac"
              color={color}
              size={size}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTab;
