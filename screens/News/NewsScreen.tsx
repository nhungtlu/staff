import React, {useCallback, useContext, useEffect} from 'react';
import {Alert, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import COLORS from '../../api/color';
import {Hotel} from '../../api/services';
import { AuthUserContext } from '../../contexts/AuthUserProvider';
import {fetchNews} from '../../redux/newSlice';

import {AppDispatch, RootState} from '../../redux/store';
import {MainStackScreenProps} from '../../stacks/Navigation';
import NewsScreenList from './NewsScreenList';
const NewsScreen: React.FC<MainStackScreenProps<'News'>> = ({navigation}) => {
  const dispatch: AppDispatch = useDispatch();
  const {hotels, isFetching} = useSelector((state: RootState) => state?.new);

  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  const onMovieItemPress = useCallback(
    (item: Hotel) => {
      navigation.navigate('NewsDetail', {id: item.id});
    },
    [navigation],
  );

  const auth = useContext(AuthUserContext);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.viewHeader}>
          <Text style={[styles.text, styles.color]}>Moonrise Beach Resort</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert('Log Out');
            auth.setAuth(false);
          }}
          style={styles.icon}>
          <Icon name="person-outline" size={38} color={COLORS.dark} />
        </TouchableOpacity>
      </View>
      <View style={styles.viewContent}>
        <NewsScreenList
          onPress={onMovieItemPress}
          isFetching={isFetching}
          hotels={hotels}
        />
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    backgroundColor: COLORS.primary,
  },
  viewHeader: {
    paddingBottom: 15,
    backgroundColor: COLORS.primary,
  },
  text: {
    marginTop: 20,
    fontSize: 30,
    fontWeight: 'bold',
  },
  color: {
    color: COLORS.white,
  },
  viewContent: {
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.grey,
  },
  icon: {
    marginTop: 20,
  },
});
export default NewsScreen;
