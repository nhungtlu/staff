import * as React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {MainStackScreenProps} from '../../stacks/Navigation';
import ProgressCircle from 'react-native-progress-circle';
import COLORS from '../../api/color';
export const DashBoardScreenRating: React.FC<
  MainStackScreenProps<'DashBoardRating'>
> = ({}) => {
  return (
    <View style={styles.formContainer}>
      <ProgressCircle
        percent={30}
        radius={50}
        borderWidth={8}
        color={COLORS.yellow}
        shadowColor="#999"
        bgColor="#fff">
        <Text style={styles.textPercent}>{'30%'}</Text>
      </ProgressCircle>
      <View style={styles.content}>
        <Text> Guest </Text>
        <Text> Rating star </Text>
        <Text> Feed back </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    marginTop: 20,
    padding: 8,
    flex: 1,
  },
  textPercent: {fontSize: 18},
  content: {
    marginTop: 30,
    flex: 10,
  },
});
