import * as React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {MainStackScreenProps} from '../../stacks/Navigation';
import ProgressCircle from 'react-native-progress-circle';
export const DashBoardScreenNews: React.FC<
  MainStackScreenProps<'DashBoardNews'>
> = ({}) => {
  return (
    <View style={styles.formContainer}>
      <ProgressCircle
        percent={50}
        radius={50}
        borderWidth={8}
        color="#3399FF"
        shadowColor="#999"
        bgColor="#fff">
        <Text style={styles.textPercent}>{'50%'}</Text>
      </ProgressCircle>
      <View style={styles.content}>
        <Text> News </Text>
        <Text> Old New </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    marginTop: 20,
    padding: 8,
    flex: 1,
  },
  complaintButton: {
    alignItems: 'center',
    marginTop: 15,
  },
  textPercent: {fontSize: 18},
  content: {
    marginTop: 30,
    flex: 10,
  },
});
