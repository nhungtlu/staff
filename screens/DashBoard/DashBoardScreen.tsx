import React from 'react';
import {View} from 'react-native';
import {ThemeButton} from '../../components/ThemeButton';
import useThemeStyle from '../../hooks/useThemeStyle';
import {MainStackScreenProps} from '../../stacks/Navigation';

const DashBoardScreen: React.FC<MainStackScreenProps<'DashBoard'>> = ({
  navigation,
}) => {
  const theme = useThemeStyle();
  return (
    <View style={theme.backgroundStyle}>
      <ThemeButton
        buttonText="Dash Board New"
        onPress={() => {
          navigation.navigate('DashBoardNews');
        }}
      />
      <ThemeButton
        buttonText="Dash Board Receiver"
        onPress={() => {
          navigation.navigate('DashBoardReceivers');
        }}
      />
      <ThemeButton
        buttonText="Dash Board Rate"
        onPress={() => {
          navigation.navigate('DashBoardRating');
        }}
      />
    </View>
  );
};
export default DashBoardScreen;
