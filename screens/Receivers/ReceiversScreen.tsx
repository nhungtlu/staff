import React, {useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import categories from '../../api/categories';
import COLORS from '../../api/color';
import ServiceList from '../../components/ServiceList';
import {MainStackScreenProps} from '../../stacks/Navigation';

const ReceiversScreen: React.FC<MainStackScreenProps<'Receivers'>> = ({
  navigation,
}) => {
  const localPress = useCallback(
    title => {
      navigation.navigate('ReceiversDetail', {title: title});
    },
    [navigation],
  );
  return (
    <View style={styles.container}>
      <View>
        <FlatList
          data={categories.items}
          renderItem={({item}) => (
            <ServiceList
              category={item}
              onPress={() => {
                localPress(item.title);
              }}
            />
          )}
        />
      </View>
    </View>
  );
};

export default ReceiversScreen;
const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    width: '100%',
    height: '90%',
    backgroundColor: COLORS.light,
    borderRadius: 20,
  },
});
