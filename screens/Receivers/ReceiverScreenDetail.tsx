import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {MainStackScreenProps} from '../../stacks/Navigation';

const ReceiversScreenDetail: React.FC<MainStackScreenProps<'ReceiversDetail'>> =
  props => {
    React.useEffect(() => {
      props.navigation.setOptions({title: props.route.params?.title});
    }, [props.navigation, props.route.params]);
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Hi</Text>
      </View>
    );
  };
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  text: {
    fontSize: 15,
    margin: 10,
  },
});
export default ReceiversScreenDetail;
