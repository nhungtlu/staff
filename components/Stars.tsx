import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {View, Text, StyleSheet} from 'react-native';
import COLORS from '../api/color';

export type Props = {
  votes: number;
  size: number;
  color: string;
};
export const StarsElements = ({votes, size, color}: Props) => {
  const starsNumber = votes;
  const star = () => {
    const starElements = [];
    for (let i = 0; i < 5; i++) {
      starElements.push(
        <Icon
          key={`star-${i}`}
          name="star"
          size={size}
          color={starsNumber > i ? color : COLORS.orange}
          style={styles.star}
        />,
      );
      return starElements;
    }
  };
  return (
    <View style={styles.wrapper}>
      <View style={styles.stars}>
        {star()}
        {votes ? <Text style={styles.votesNumber}>{votes}</Text> : null}
      </View>
    </View>
  );
};
export default StarsElements;

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  star: {
    marginRight: 1,
  },
  stars: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  votesNumber: {
    fontSize: 11,
    fontWeight: '600',
    marginTop: 1,
    marginLeft: 3,
  },
});
