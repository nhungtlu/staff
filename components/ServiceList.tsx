﻿import * as React from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface ServiceListProps {
  category: {
    id: string;
    title: {
      id: string;
    }[];
  };
}

const ServiceList = (props: ServiceListProps) => {
  const {category} = props;

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onPress}>
        <Text style={styles.title}>{category.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ServiceList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    margin: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
