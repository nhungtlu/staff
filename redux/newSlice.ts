import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import newAPI, {Hotel} from '../api/services';

export const fetchNews = createAsyncThunk<Hotel[]>(
  'news/fetchAll',
  async () => {
    const response = await newAPI.fetchAll();
    // console.log(response, 'ccccccccccccccccccc');

    return response?.results;
  },
);

export const fetchNew = createAsyncThunk<Hotel, number>(
  'news/fetchDetail',
  async (id: number) => {
    const response = await newAPI.fetchDetail(id);
    return response;
  },
);

type NewState = {
  isFetching: boolean;
  hotels: Hotel[];
  error: any;
  isFetchingDetail?: boolean;
  hotel?: Hotel;
  errorDetail?: any;
};
const initialState: NewState = {
  isFetching: false,
  hotels: [],
  error: null,
};

export const slice = createSlice({
  name: 'hotel',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchNews.pending, (state, _action) => {
      state.isFetching = true;
    });
    builder.addCase(fetchNews.fulfilled, (state, action) => {
      state.hotels = action.payload;
      state.isFetching = false;
    });
    builder.addCase(fetchNews.rejected, (state, action) => {
      state.error = action.payload;
      state.isFetching = false;
    });
    builder.addCase(fetchNew.pending, (state, _action) => {
      state.isFetchingDetail = true;
    });
    builder.addCase(fetchNew.fulfilled, (state, action) => {
      state.hotel = action.payload;
      state.isFetchingDetail = false;
    });
    builder.addCase(fetchNew.rejected, (state, action) => {
      state.errorDetail = action.payload;
      state.isFetchingDetail = false;
    });
  },
});

export default slice.reducer;
