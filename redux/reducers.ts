import {combineReducers} from 'redux';
import newsReducer from './newSlice';
const rootReducer = combineReducers({
  new: newsReducer,
});

export default rootReducer;
